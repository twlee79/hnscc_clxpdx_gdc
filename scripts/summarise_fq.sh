#!/bin/sh

# count total reads and total bases in a fastq.gz files
# if multiple files provided, will return sum

gzip -dc $@ |
     awk 'NR%4==2{c++; l+=length($0)}
          END{
                print "total_reads\ttotal_bases"
                print c,"\t",l
              }'
